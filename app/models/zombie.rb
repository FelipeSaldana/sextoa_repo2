class Zombie < ApplicationRecord
   
    
    has_many :brains, dependent: :destroy
    validates :name, presence: true
    validates :bio, length: { maximum: 10 }
    validates :age, numericality: {only_integer: true, message: "solo numeros enteros"}
    validates :email, format: {with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i, message: "formato requerido"}
    mount_uploader :avatar, AvatarUploader

end
